import annotations.State;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(InfiniTestRunner.class)
public class InfiniTest {

    @Test
    public void initInfiniTest() {}

    @State(in = "Start", weight = 0.001, out = "Middle")
    public static void startMiddle(){}

    @State(in = "Start", weight = 0.001, out = "Finish")
    public static void startFinish() {
        Assert.assertTrue(false);
    }

    @State(in = "Start", weight = 100, out = "Middle")
    public static void startMiddle2(){}

    @State(in = "Middle", weight = 1, out = "Middle")
    public static void middleMiddle2(){}

    @State(in = "Middle", weight = 0.5, out = "Finish")
    public static void middleFinish2(){}

    @State(in = "Middle", weight = 0.5, out = "Finish")
    public static void middleFinish3() {
        Assert.assertTrue(false);
    }

}
