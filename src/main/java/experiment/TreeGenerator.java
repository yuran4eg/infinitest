package experiment;

import annotations.State;
import model.Node;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static model.TestStep.INITIAL_STATE;

public class TreeGenerator {

    private List<Node<Method>> nodeList = new ArrayList<Node<Method>>();

    private void getMethodsAnnotatedWith(final Class type, String currentState, Node<Method> currentNode) {
        final List<Method> allMethods = new ArrayList<Method>(Arrays.asList(((Class<?>) type).getDeclaredMethods()));
        for (final Method method : allMethods) {
            if (method.isAnnotationPresent(State.class)) {
                State annotInstance = method.getAnnotation(State.class);
                if (annotInstance.in().equalsIgnoreCase(currentState)) {
                    Node<Method> newNode = new Node<Method>(method);
                    if (currentState.equalsIgnoreCase(INITIAL_STATE)) {
                        nodeList.add(newNode);
                    } else {
                        currentNode.addChild(newNode);
                    }
                    getMethodsAnnotatedWith(type, annotInstance.out(), newNode);
                }
            }
        }
    }

    public List<Node<Method>> getNodeList() {
        return nodeList;
    }
}
