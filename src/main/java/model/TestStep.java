package model;

import annotations.State;

import java.lang.reflect.Method;

public class TestStep {

    public final static String INITIAL_STATE = "start";
    public final static String FINISH_STATE = "finish";

    private Object testObject;
    private Method method;
    private String currentState;
    private String upcomingState;
    private double weight;

    public TestStep(Object testObject, Method method) {
        this.testObject = testObject;
        this.method = method;
        State annotInstance = method.getAnnotation(State.class);
        this.currentState = annotInstance.in();
        this.upcomingState = annotInstance.out();
        this.weight = annotInstance.weight();
    }

    public Object getTestObject() {
        return testObject;
    }

    public Method getMethod() {
        return method;
    }

    public String getUpcomingState() {
        return upcomingState;
    }

    public String getCurrentState() {
        return currentState;
    }

    public double getWeight() {
        return weight;
    }
}
