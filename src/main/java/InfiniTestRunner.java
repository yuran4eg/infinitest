import model.TestStep;
import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;
import services.TestLoader;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.StringJoiner;

import static services.TestExecutor.executeTestScenario;
import static services.TestExecutor.generateTestScenario;

public class InfiniTestRunner extends Runner {

    private Class testClass;

    private final static String CLASS_NAME = "InfiniTest";
    private final static String UNIT_TIME = "time";
    private final static String UNIT_TEST = "test";

    private String unit = System.getProperty("infinitest.unit");
    private String duration = System.getProperty("infinitest.duration");


    private RunNotifier notifier;
    private List<TestStep> allTestSteps;

    public InfiniTestRunner(Class testClass) {
        super();
        this.testClass = testClass;
    }

    @Override
    public Description getDescription() {
        return Description
                .createTestDescription(testClass, CLASS_NAME);
    }

    @Override
    public void run(RunNotifier notifier) {
        this.notifier = notifier;
        this.allTestSteps = TestLoader.getTestSteps(testClass);
        switch (unit){
            case UNIT_TIME: runTestsForTime();break;
            case UNIT_TEST: runTestsForTestNumber();break;
            default: System.out.println(unit + " - unexpected unit. Possible values: "+ UNIT_TIME + ", " + UNIT_TEST);
        }
    }

    private void runTestsForTime(){
        int intDuration = Integer.parseInt(duration);
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime <= intDuration * 1000) {
            runTest();
        }
    }
    private void runTestsForTestNumber(){
        int intDuration = Integer.parseInt(duration);
        while(intDuration>0){
            runTest();
            intDuration--;
        }
    }

    private void runTest(){
        List<TestStep> testScenario = generateTestScenario(allTestSteps);
        StringJoiner descriptionBuilder = new StringJoiner("-");
        testScenario.forEach(s -> descriptionBuilder.add(s.getMethod().getName()));
        Description description = Description.createTestDescription(CLASS_NAME, descriptionBuilder.toString());

        try {
            notifier.fireTestStarted(description);
            executeTestScenario(testScenario);
            notifier.fireTestFinished(description);
        } catch (InvocationTargetException e) {
            testFailed(description, e.getCause());
        } catch (Exception e) {
            testFailed(description, e);
        }
    }

    private void testFailed(Description description,Throwable e) {
        notifier.fireTestFailure(new Failure(description, e));
        notifier.fireTestFinished(description);
    }
}



