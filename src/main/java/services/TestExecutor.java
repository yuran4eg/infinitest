package services;

import model.TestStep;
import org.apache.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import static model.TestStep.FINISH_STATE;
import static model.TestStep.INITIAL_STATE;

public class TestExecutor {

    private final static Logger logger = Logger.getLogger(TestExecutor.class);

    public static List<TestStep> generateTestScenario(List<TestStep> testSteps) {

        String currentState = INITIAL_STATE;
        List<TestStep> scenarioSteps = new ArrayList<>();
        while (!currentState.equalsIgnoreCase(FINISH_STATE)) {
            TestStep step = null;
            try {
                step = getRandomNextStep(testSteps, currentState);
                scenarioSteps.add(step);
                currentState = step.getUpcomingState();
            } catch (MethodNotFoundException e) {
                logger.error(e.getStackTrace());
                break;
            }
        }
        return scenarioSteps;
    }

    public static void executeTestScenario(List<TestStep> scenario) throws InvocationTargetException, IllegalAccessException {
        for (TestStep step : scenario) {
            TestExecutor.executeStep(step);
        }
    }

    private static TestStep getRandomNextStep(List<TestStep> testSteps, String state) throws MethodNotFoundException {
        List<TestStep> resultList = testSteps.stream()
                .filter(step -> step.getCurrentState().equalsIgnoreCase(state))
                .collect(Collectors.toList());
        double sumWeight = resultList.stream().mapToDouble(TestStep::getWeight).sum();
        double randomDouble = ThreadLocalRandom.current().nextDouble(0, sumWeight);
        double currentShift = 0;
        for (TestStep step : resultList) {
            if (step.getWeight() + currentShift >= randomDouble) return step;
            else currentShift += step.getWeight();

        }
        throw new MethodNotFoundException();
    }

    private static void executeStep(TestStep step) throws InvocationTargetException, IllegalAccessException {
            step.getMethod().invoke(step.getTestObject());
    }
}

class MethodNotFoundException extends Exception {

};
