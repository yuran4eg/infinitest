package services;

import annotations.State;
import annotations.TestClass;
import model.TestStep;
import org.apache.log4j.Logger;
import org.reflections.Reflections;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;


public class TestLoader {

    private final static Logger logger = Logger.getLogger(TestExecutor.class);

    private final static Class<? extends Annotation> testAnnotation = State.class;
    private final static Class<? extends Annotation> classAnnotation = TestClass.class;

    public static List<TestStep> getTestSteps(String packageName) {
        Reflections reflections = new Reflections(packageName);
        Set<Class<?>> testClasses = reflections.getTypesAnnotatedWith(classAnnotation);
        List<TestStep> list = new ArrayList<>();
        for (Class testClass : testClasses) {
            list.addAll(getTestSteps(testClass));
        }
        return list;
    }

    public static List<TestStep> getTestSteps(Class className) {
        List<TestStep> list = new ArrayList<>();
        List<Method> methods = getMethodsAnnotatedWith(className);
        for (Method method : methods) {
            try {
                TestStep testStep = new TestStep(className.newInstance(), method);
                list.add(testStep);
            } catch (InstantiationException | IllegalAccessException e) {
                logger.error("Can not create instance of class " + method.getDeclaringClass());
                logger.error(e.getStackTrace());
            }
        }
        return list;
    }

    private static List<Method> getMethodsAnnotatedWith(Class<?> type) {
        List<Method> methods = new ArrayList<>();
        List<Method> allMethods = new ArrayList<>(Arrays.asList(type.getDeclaredMethods()));
        for (Method method : allMethods) {
            if (method.isAnnotationPresent(testAnnotation)) {
                methods.add(method);
            }
        }
        return methods;
    }
}
