import model.TestStep;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import services.TestLoader;

import java.util.List;

@Mojo(name = "infTest")
public class InfiniTestMain extends AbstractMojo {

    @Parameter(property = "module")
    private String module;

    @Parameter(property = "time", defaultValue = "0")
    private Integer time;

    @Parameter(property = "number", defaultValue = "0")
    private Integer number;

    public static void main(String[] args) {
        String module = "";
        int number = 5;
        int time = 1;
        List<TestStep> testSteps = TestLoader.getTestSteps(module);
//        TestExecutor.startTestingFixedNumber(testSteps, number);
//        TestExecutor.startTestingFixedTime(testSteps,arg.getTime());
    }

    @Override
    public void execute() {
//        List<TestStep> testSteps = TestLoader.getTestSteps(module);
//        logger.debug("testSteps.size() " + testSteps.size());
//        TestExecutor.startTestingFixedNumber(testSteps, number);

    }
}
