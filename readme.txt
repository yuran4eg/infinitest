-to generate and execute 50 tests
mvn clean test -Dinfinitest.unit=test -Dinfinitest.duration=50

-to generate and execute tests for 1 sec
mvn clean test -Dinfinitest.unit=time -Dinfinitest.duration=1

